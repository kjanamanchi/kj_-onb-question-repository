==========
Objective: Write the code to create a table
==========

[[
You are given the following requirements for a <code>User</code> database table:
<br/>
<blockquote>
	<table>
		<tbody>
			<tr>
				<th style="text-align:right">username:&nbsp;</th>
				<td>a unique string of at most fifty characters; it is required</td>
			</tr>
			<tr>
				<th style="text-align:right">state:&nbsp;</th>
				<td>a string of exactly two characters; it might be empty</td>
			</tr>
			<tr>
				<th style="text-align:right">age:&nbsp;</th>
				<td>an integer from zero to one hundred; it might be empty</td>
			</tr>
		</tbody>
	</table>
</blockquote>
<br/>
Which SQL-92 DDL statement accurately satisfies these requirements?
]]
1: <pre>CREATE TABLE User (
   username STRING(50) UNIQUE,
   stateCode STRING(2),
   age SMALLINT
)</pre>
2: <pre>CREATE TABLE User (
   username STRING(50) UNIQUE,
   stateCode STRING(2),
   age INT(2)
)</pre>
*3: <pre>CREATE TABLE User (
   username VARCHAR(50) UNIQUE,
   stateCode CHAR(2),
   age SMALLINT
)</pre>
4: <pre>CREATE TABLE User (
   username VARCHAR(50) NOT NULL UNIQUE,
   stateCode CHAR(2) NULL,
   age INT(2) NULL
)</pre>


==========
Objective: Given a scenario, write the code to generate an index to solve
 the performance problem identify in the scenario
==========

[[
Given the following database table definitions:

<pre><strong>OrderLineItem</strong>
orderId: string,     // key
customerId: string,  // one customer per order
productCode: string, // one product per line-item
quantity: integer</pre>

and:

<pre><strong>Product</strong>
productCode: string, // key
unitPrice: decimal</pre>

The following <em>pseudo-code</em> query is not performing very well:

<pre>retrieve add(unitPrice * quantity)
from OrderLineItem and Product
when orderId = ?
</pre>

Which MySQL DDL statement could solve the performance issue?
]]
1: <code>ALTER TABLE OrderLineItem CREATE INDEX (orderId, productCode);</code>
*2: <code>CREATE INDEX OrderProductIdx ON OrderLineItem (orderId, productCode);</code>
3: <code>ALTER TABLE OrderLineItem CREATE INDEX (orderId, customerId, productCode);</code>
4: <code>CREATE INDEX OrderProductIdx ON OrderLineItem (orderId, customerId, productCode);</code>

[[NEW ITEM: rewrite the above to have them understand the concept of creating an index]]

[[
Given the following database table definitions:

<pre><strong>OrderLineItem</strong>
orderId: string,     // key
customerId: string,  // one customer per order
productCode: string, // one product per line-item
quantity: integer</pre>

and:

<pre><strong>Product</strong>
productCode: string, // key
unitPrice: decimal</pre>

The following <em>pseudo-code</em> query is not performing very well:

<pre>retrieve add(unitPrice * quantity)
from OrderLineItem and Product
when orderId = ?
</pre>

What can you do to resolve the performance issue?
]]
1:  Create a stored procedure the optimize this query
2:  Partition the <code>OrderLineItem</code> table on the <code>orderId</code> field
*3: Create an index on orderId for the <code>OrderLineItem</code> table
4:  Increase the memory allocation for the database process
