(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
0 Generic
1 Java
2 Axure
3 JIRA
4 OOP
5 SDLC
6 Communication
7 Unix Scripting
8 HTML
9 CSS
10 JavaScript
11 Angular JS
12 Design Patterns
13 Git

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter): Brad Francis
(Course Site): Lynda
(Course Name): CSS Fundamentals
(Course URL): http://www.lynda.com/Web-Interactive-CSS-tutorials/CSS-Fundamentals/80436-2.html
(Discipline): CSS
(ENDIGNORE)

(Type): multiplechoice
(Category): 9
(Grade style): 0
(Random answers): 1
(Question): What version of CSS was never fully implemented in web browsers?
(A): CSS 1.0
(B): CSS 1.5
(C): CSS 2.0
(D): CSS 2.1
(Correct): C
(Points): 1
(CF): CSS 2.0 was never fully implemented in web browsers.
(WF): CSS 2.0 was never fully implemented in web browsers.
(STARTIGNORE)
(Hint): 
(Subject): CSS	
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 9
(Grade style): 0
(Random answers): 1
(Question): What will the following CSS do?
	p { color: black }
(A): Apply black color to all pages.
(B): It will do nothing.
(C): Apply a color of black to all paragraph tags.
(D): Apply a color of black to all paragraph tags and their children if the children do not specify their own.
(Correct): D
(Points): 1
(CF): It will apply a color of black to all paragraphs and their children if the children have not specified their own color.
(WF): It will apply a color of black to all paragraphs and their children if the children have not specified their own color.
(STARTIGNORE)
(Hint):
(Subject): CSS
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 9
(Grade style): 0
(Random answers): 1
(Question): In the following snippet of CSS, what font will be attempted to be used if arial is not installed on the client's machine?
	p { font-family: Arial, Times New Roman, Veranda, sans-serif; }
(A): Times New Roman
(B): Veranda
(C): Courier New
(D): An error is thrown.
(Correct): A
(Points): 1
(CF): If the client's machine does not have Arial installed the next font in line Times New Roman will be the next font attempted.
(WF): If the client's machine does not have Arial installed the next font in line Times New Roman will be the next font attempted.
(STARTIGNORE)
(Hint):
(Subject): CSS
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 9
(Grade style): 1
(Random answers): 1
(Question): What are some examples of CSS positioning schemes? (Select all that apply)
(A): Normal Flow
(B): Element Floating
(C): Boxing
(D): Absolute Positioning
(E): Right Most Justified
(Correct): A,B,D
(Points): 1
(CF): Some examples of css positioning are: Normal flow, element floating, and absolute positioning.
(WF): Some examples of css positioning are: Normal flow, element floating, and absolute positioning.
(STARTIGNORE)
(Hint): 
(Subject): CSS
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 9
(Grade style): 1
(Random answers): 1
(Question): What are some good frameworks that are recommended to help with css layouts? (Select all that apply)
(A): 52 Framework
(B): Bluepoint
(C): Chromeish
(D): Super Awesome CSS
(E): YUI 2
(Correct): A,B,E
(Points): 1
(CF): The course suggests in using the following frameworks to help with css development: 52 framework, bluepoint, and YUI 2.
(WF): The course suggests in using the following frameworks to help with css development: 52 framework, bluepoint, and YUI 2.
(STARTIGNORE)
(Hint):
(Subject): CSS
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 9
(Grade style): 1
(Random answers): 1
(Question): Why is it not always ideal to try and use pre-defined frameworks for your CSS? (Select all that apply)
(A): They do not help, they just get in the way.
(B): A lot of sites do not use all the features of the framework.
(C): They often have class heavy non semantic markup.
(D): Frameworks are only designed to work with one type of site.
(E): Adding functionality can be time-consuming
(Correct): B,C,E
(Points): 1
(CF): Frameworks are not always ideal because: most sites do not use all of the features, they usually have class heavy non semantic markup, and adding functionality can be time consuming. 
(WF): Frameworks are not always ideal because: most sites do not use all of the features, they usually have class heavy non semantic markup, and adding functionality can be time consuming.
(STARTIGNORE)
(Hint):
(Subject): CSS
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)
