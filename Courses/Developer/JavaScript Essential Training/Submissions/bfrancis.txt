(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
0 Generic
1 Java
2 Axure
3 JIRA
4 OOP
5 SDLC
6 Communication
7 Unix Scripting
8 HTML
9 CSS
10 JavaScript
11 Angular JS
12 Design Patterns
13 Git
14 Java Design Pattern
15 Business Analysis
16 Version Control
17 Presentations
18 Software Testing
19 Foundations of Programming: Software Quality Assurance
20 PowerPoint 2013 Essential Training
21 SQL Essential Training
22 Web Design Fundamentals

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter): Brad Francis
(Course Site): lynda.com
(Course Name): JavaScript Essential Training
(Course URL): http://www.lynda.com/JavaScript-tutorials/JavaScript-Essential-Training/81266-2.html
(Discipline): Technical
(ENDIGNORE)


(Type): multiplechoice
(Category): 10
(Grade style): 0
(Random answers): 1
(Question): What is javascript designed for doing?
(A): I/O operations
(B): Database operations
(C): Manipulating web pages
(D): Creating a script based workflow system
(Correct): C
(Points): 1
(CF): JS was created to manipulate web pages.
(WF): JS was created to manipulate web pages.
(STARTIGNORE)
(Hint):
(Subject): JavaScript Essential Training
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 10
(Grade style): 1
(Random answers): 1
(Question): Which of the following can a javascirpt variable consist of? (Select all that apply)
(A): Letters
(B): Numbers
(C): Spaces
(D): Underscores
(E): $
(Correct): A,B,D,E
(Points): 1
(CF): JS can consist of letters, numbers, underscores and $.
(WF): JS can consist of letters, numbers, underscores and $.
(STARTIGNORE)
(Hint):
(Subject): JavaScript Essential Training
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 10
(Grade style): 0
(Random answers): 1
(Question): What will the following snippet of code evaluate to?
	if ( c === 99 ) {
	}
(A): Check if c equals 99
(B): Check if c equals 99 and the type is a number
(C): Check if c equals 99 if not, set it to 99
(D): Check if c is not equal to 99
(Correct): B
(Points): 1
(CF): === checks both value and type.
(WF): === checks both value and type.
(STARTIGNORE)
(Hint):
(Subject): JavaScript Essential Training
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 10
(Grade style): 1
(Random answers): 1
(Question): Which are true about the scope of a variable in javascript? (Select all that apply)
(A): A variable defined outside of a function is global
(B): A variable defined within a function is local to that function only
(C): A variable defined within a loop is global
(D): A variable defined within a loop is local to that loop
(E): All variables are global
(Correct): A,B,C
(Points): 1
(CF): Variables in java script; defined outside of a function is global, defined within a function is local to the function only, and defined within a loop is global.
(WF): Variables in java script; defined outside of a function is global, defined within a function is local to the function only, and defined within a loop is global.
(STARTIGNORE)
(Hint):
(Subject): JavaScript Essential Training
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 10
(Grade style): 0
(Random answers): 1
(Question): What will the following snippet of code output?
	var x ="10";
	var y = 4;
	console.log(x + y);
(A): 14
(B): NaN(not a number)
(C): 104
(D): Undefined
(Correct): C
(Points): 1
(CF): Javascript will treat any evaluation as a concatenation if there are any strings involved.
(WF): Javascript will treat any evaluation as a concatenation if there are any strings involved.
(STARTIGNORE)
(Hint):
(Subject): JavaScript Essential Training
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 10
(Grade style): 1
(Random answers): 1
(Question): Which of the below are valid events for javascript? (Select all that apply)
(A): onup
(B): onload
(C): onclick
(D): onfocus
(E): onpress
(Correct): B,C,D
(Points): 1
(CF): The valid events from the answers provided are: onload, onclick, and onfocus.
(WF): The valid events from the answers provided are: onload, onclick, and onfocus.
(STARTIGNORE)
(Hint):
(Subject): JavaScript Essential Training
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)
