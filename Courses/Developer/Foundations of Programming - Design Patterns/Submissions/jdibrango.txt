(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
0 Generic
1 Java
2 Axure
3 JIRA
4 OOP
5 SDLC
6 Communication
7 Unix Scripting
8 HTML
9 CSS
10 JavaScript
11 Angular JS
12 Design Patterns
13 Git
14 Java Design Pattern
15 Business Analysis
16 Version Control
17 Presentations

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter): Joseph DiBrango
(Course Site): www.lynda.com
(Course Name): Foundations of Programming: Design Patterns
(Course URL) : http://www.lynda.com/Developer-Programming-Foundations-tutorials/Foundations-Programming-Design-Patterns/135365-2.html
(Discipline): Technical 
(ENDIGNORE)

(Type): truefalse
(Category): 12
(Grade style): 0
(Random answers): 1
(Question): The primary goal of any Design Pattern is to help you structure your code so it is flexible and resilient?
(A): True
(B): False
(Correct): A
(Points): 1
(CF): Following a design patterns allows your code to become more flexible and resilient.
(WF): Following a design patterns allows your code to become more flexible and resilient.
(STARTIGNORE)
(Hint):
(Subject): Design Patterns
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 12 
(Grade style): 0
(Random answers): 1
(Question): Is-a is known as a _____ between abstractions?
(A): Composition
(B): Relationship
(C): Pattern
(D): De-coupling
(Correct): A
(Points): 1
(CF): Is-a is a relationship between abstractions.
(WF): Is-a is a relationship between abstractions.
(STARTIGNORE)
(Hint):
(Subject): Design Patterns
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 12
(Grade style): 0
(Random answers): 1
(Question): The Strategy pattern is quite similar to another pattern mentioned in this course. Which one?
(A): The Singleton pattern
(B): The Iterator pattern
(C): The State pattern
(D): The Decorator pattern
(E): The Factory pattern
(Correct): C
(Points): 1
(CF): The Strategy pattern, when diagrammed, is very similar to the State pattern. However, the intent behind the use of the pattern is different.
(WF): The Strategy pattern, when diagrammed, is very similar to the State pattern. However, the intent behind the use of the pattern is different.
(STARTIGNORE)
(Hint):
(Subject): Design Patterns
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 12
(Grade style): 0
(Random answers): 1
(Question):  We always want to strive for loosely coupled designs between objects that interact?
(A): True
(B): False
(Correct): A
(Points): 1
(CF): This is true, because it gives us the flexibility to vary our design without breaking the contract between interacting objects.
(WF): This is true, because it gives us the flexibility to vary our design without breaking the contract between interacting objects.
(STARTIGNORE)
(Hint):
(Subject): Design Patterns
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 12
(Grade style): 0
(Random answers): 1
(Question): A design pattern is a solution to a problem?
(A): True
(B): False
(Correct): B
(Points): 1
(CF): A design pattern is not a solution but a description for how to solve a problem in different situations.
(WF): A design pattern is not a solution but a description for how to solve a problem in different situations.
(STARTIGNORE)
(Hint):
(Subject): Design Patterns
(Difficulty): Advanced
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 12
(Grade style): 0
(Random answers): 1
(Question): Which of the following are true of design patterns (select all that apply)?
(A): They promote a powerful shared vocabulary
(B): They are the "ultimate" in re-use, as they re-use the experience of prior developers
(C): They are guidelines for design
(D): They should be used in all programming projects
(E): They promote code flexibility
(Correct): A,B,C,E
(Points): 1
(CF): Patterns are very useful, but they should only be used when they fit the problem at hand and the project is likely to experience some degree of change so they are not needed in all programming projects.
(WF): Patterns are very useful, but they should only be used when they fit the problem at hand and the project is likely to experience some degree of change so they are not needed in all programming projects.
(STARTIGNORE)
(Hint):
(Subject): Design Patterns
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 12
(Grade style): 0
(Random answers): 1
(Question): The Open-Closed principle states that (select all that apply)?
(A): We favor tightly coupled classes
(B): Classes should be closed for modification
(C): Classes have many reasons for change
(D): Classes are open to extensions of behavior
(Correct): B,D
(Points): 1
(CF): Open-Close principle - Classes should be open to extension but closed for modification.
(WF): Open-Close principle - Classes should be open to extension but closed for modification.
(STARTIGNORE)
(Hint):
(Subject): Design Patterns
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 12
(Grade style): 0
(Random answers): 0
(Question): A class should have many reasons to change?
(A): True
(B): False
(Correct): B
(Points): 1
(CF): False - Design principle #6 - A class should have only one reason to change.
(WF): False - Design principle #6 - A class should have only one reason to change.
(STARTIGNORE)
(Hint):
(Subject): Design Patterns
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 12
(Grade style): 0
(Random answers): 1
(Question): Defines an interface for creating an object, but lets the subclasses decide which class to instantiate?
(A): Singleton Pattern
(B): Strategy Pattern
(C): Factory Pattern
(D): Decorator Pattern
(Correct): C
(Points): 1
(CF): The Factory Pattern defines an interface for creating an object, but lets the subclasses decide which class to instantiate.
(WF): The Factory Pattern defines an interface for creating an object, but lets the subclasses decide which class to instantiate.
(STARTIGNORE)
(Hint):
(Subject): Design Patterns
(Difficulty): Advanced
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 12
(Grade style): 0
(Random answers): 1
(Question): Defines a family of algorithms, encapsulate each one, and make them interchangeable?
(A): Singleton Pattern
(B): Strategy Pattern
(C): Factory Pattern
(D): Decorator Pattern
(Correct): B
(Points): 1
(CF): The Strategy Pattern defines a family of algorithms, encapsulate each one, and make them interchangeable.
(WF): The Strategy Pattern defines a family of algorithms, encapsulate each one, and make them interchangeable.
(STARTIGNORE)
(Hint):
(Subject): Design Patterns
(Difficulty): Advanced
(Applicability): Course
(ENDIGNORE)