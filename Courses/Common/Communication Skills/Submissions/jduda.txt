(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
0 Generic
1 Java
2 Axure
3 JIRA
4 OOP
5 SDLC
6 Communication
7 Unix Scripting
8 HTML
9 CSS
10 JavaScript
11 Angular JS
12 Design Patterns
13 Git
14 Java Design Pattern
15 Business Analysis
16 Version Control
17 Presentations
18 Software Testing
19 Foundations of Programming: Software Quality Assurance
20 PowerPoint 2013 Essential Training
21 SQL Essential Training
22 Web Design Fundamentals

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter): Joseph Duda
(Course Site): Udemy
(Course Name): Consulting Skills Series Communication
(Course URL): https://www.udemy.com/consulting-skills-series-communication/
(Discipline): Professional
(ENDIGNORE)

(Type): multiplechoice
(Category): 6
(Grade style): 0
(Random answers): 1
(Question): Which of the following can help you connect with your audience while giving a presentation?
(A): Telling stories that they can relate to.
(B): Imagining they are all sitting in their underwear.
(C): Being honest if you're nervous giving the presentation.
(D): Giving them handouts in case they can't read your slides.
(E): Smiling and giving eye contact.
(Correct): A, C, E
(Points): 1
(CF): Imagining them in their underwear doesn't help you connect. Giving them handouts will distance them from you as they now have less need to pay attention.
(WF): Imagining them in their underwear doesn't help you connect. Giving them handouts will distance them from you as they now have less need to pay attention.
(STARTIGNORE)
(Hint): Put yourself in the audience's shoes.
(Subject): Communication skills
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 6
(Grade style): 0
(Random answers): 1
(Question): Which of the following are good ground rules to set for a meeting?
(A): Stay on topic.
(B): Only answer urgent phone calls.
(C): Show mutual respect.
(D): Focus on the meeting - no note taking allowed.
(E): Shut off phones.
(Correct): A,D,E
(Points): 1
(CF): Good ground rules are to stay on topic, use a "parking lot", show mutual respect, no questions are stupid, shut off phones, and speak up if you need a break.
(WF): Good ground rules are to stay on topic, use a "parking lot", show mutual respect, no questions are stupid, shut off phones, and speak up if you need a break.
(STARTIGNORE)
(Hint): Two of the answers conflict with each other.
(Subject): Communication skills
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 6
(Grade style): 0
(Random answers): 1
(Question): If you are NOT actively contributing to a conference call, you should use the mute button.
(A): True
(B): False
(Correct): A
(Points): 1
(CF): Even background static can be annoying on a conference call.
(WF): Even background static can be annoying on a conference call.
(STARTIGNORE)
(Hint):
(Subject): Communication skills
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 6
(Grade style): 0
(Random answers): 1
(Question): What percent of Powerpoint presentations would most people consider to be downright bad?
(A): 0%
(B): 25%
(C): 50%
(D): 75%
(E): 100%
(Correct): C
(Points): 1
(CF): Half of all powerpoint presentations could use serious improvement.
(WF): Half of all powerpoint presentations could use serious improvement.
(STARTIGNORE)
(Hint): It's not 0!
(Subject): Communication skills
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 6
(Grade style): 0
(Random answers): 1
(Question): Convert or contain refers to which of the following?
(A): Teaching technologically illiterate users how to use Microsoft Excel.
(B): Changing a ZIP file into a RAR file.
(C): A method for preventing office gossip.
(D): A way to replay recorded conference calls.
(E): Preventing adversaries from interfering with your success.
(Correct): E
(Points): 1
(CF): You want to convert your adversaries into allies or contain their adversarial ways.
(WF): You want to convert your adversaries into allies or contain their adversarial ways.
(STARTIGNORE)
(Hint): Most of the choices have little to do with person to person communication.
(Subject): Communication skills
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)
