(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
2 Axure
6 Communication
0 Generic
8 HTML
1 Java
3 JIRA
4 OOP
5 SDLC
7 Unix Scripting

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter): Terry Egan
(Course Site): Udemy
(Course Name): Communication Skills
(Course URL): https://www.udemy.com/consulting-skills-series-communication/
(Discipline): Professional
(ENDIGNORE)

(Type): multiplechoice
(Category): 6
(Grade style): 0
(Random answers): 1
(Question): What is the most important component to being a successful consultant?
(A): Being an expert.
(B): Being organized.
(C): Trust
(D): Being charismatic.
(E): Sending emails.
(Correct): C
(Points): 1
(CF): The training course's main theme is that Trust is the key component to being a successful consultant.
(WF): The training course's main theme is that Trust is the key component to being a successful consultant.
(STARTIGNORE)
(Hint):
(Subject): Client Relationship
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 6
(Grade style): 0
(Random answers): 1
(Question): What is the most important part about sending email communications?
(A): Including every detail.
(B): Being succinct and to the point.
(C): Making sure you CC everything to your manager.
(D): Including your displeasure in your email, so you can convey yourself.
(E): Neglecting to include your phone number or contact information.
(Correct): B 
(Points): 1
(CF): Being succinct and to the point helps with conveying the facts, ensures your message was understood, and respects the recipient's time.
(WF): Being succinct and to the point helps with conveying the facts, ensures your message was understood, and respects the recipient's time.
(STARTIGNORE)
(Hint):
(Subject): Email Communications
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 6
(Grade style): 0
(Random answers): 1
(Question): Which statement is not true about sending email communications?
(A): Email messages are not legally binding and cannot be used as evidence.
(B): Being succinct and to the point is a good practice.
(C): Including your contact information such as including your name, phone number is a good practice.
(D): Being polite and using words such as please and thank you is a good practice with sending emails.
(E): Proof-reading your email before you press the send button is a good practice.
(Correct): A
(Points): 1
(CF): Email messages are legal documents and can be used as evidence.
(WF): Email messages are legal documents and can be used as evidence.
(STARTIGNORE)
(Hint):
(Subject): Email Communications
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 6
(Grade style): 0
(Random answers): 1
(Question): According to the course instructor, which of the following is not a true statement about presentations?
(A): You should plan your presentation by using a pencil and paper.
(B): You should include only one point per slide.
(C): When performing presentation with an executive it is critical to know that time is their most precious asset.
(D): Executives love Power Point presentations.
(E): Executives are human, and have fears.
(Correct): D
(Points): 1
(CF): According to the instructor, executives don't like Power Point presentations, because most presenters don't prepare for them sufficiently.  This results in wasted time.
(WF): According to the instructor, executives don't like Power Point presentations, because most presenters don't prepare for them sufficiently.  This results in wasted time.
(STARTIGNORE)
(Hint):
(Subject): Presentations
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 6
(Grade style): 0
(Random answers): 1
(Question): Which statement is true about conference calls?
(A): You don't need to mute your phone if you are not participating in the conference call.
(B): Establishing simple Ground Rules toward the beginning of the conference call is a good practice.
(C): It is a good practice to take notes using your laptop.
(D): A meeting agenda is not required.
(E): It is acceptable to call in using your cell phone.
(Correct): B 
(Points): 1
(CF): Establishing simple Ground Rules helps ensure the meeting objectives are met and distractions don't occur.
(WF): Establishing simple Ground Rules helps ensure the meeting objectives are met and distractions don't occur.
(STARTIGNORE)
(Hint):
(Subject): Conference Calls
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 6
(Grade style): 0
(Random answers): 1
(Question): During a conference call that you organized, which of the following is the least accurate?
(A): Your audience has a long attention span.
(B): At the beginning of the meeting, review the meeting agenda.
(C): Avoid using Power Point slides.
(D): Keep your statements short, and ask for frequent feedback.
(E): Assume the participants did not read the agenda.
(Correct): A
(Points): 1
(CF): According to the Instructor, an adult has an average attention-span of 8 minutes.
(WF): According to the Instructor, an adult has an average attention-span of 8 minutes.
(STARTIGNORE)
(Hint):
(Subject): Conference Calls
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 6
(Grade style): 0
(Random answers): 1
(Question): Which of the following is not a good practice to use when communicating with an executive?
(A): Be very detailed with the explanation.
(B): Deliver bottom line results first, then summarize.
(C): Keep the message simple.
(D): Know your elevator pitch.
(E): Put yourself in the executive's shoes.
(Correct): A 
(Points): 1
(CF): According to the Course Instructor, an executive's time is their most precious asset.
(WF): According to the Course Instructor, an executive's time is their most precious asset.
(STARTIGNORE)
(Hint):
(Subject): Presentations
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 6
(Grade style): 0
(Random answers): 1
(Question):  Select the best technique to use for people who get off topic in a meeting?
(A): If the participant violates this rule three times, then don't allow that person to speak anymore.
(B): Use the "Parking Lot" technique.
(C): Make sure that person does not get invited to future meetings.
(D): Report the problem to their manager.
(E): Kindly ask the participant to leave.
(Correct): B
(Points): 1
(CF): The "Parking Lot" is a technique that is used to capture topics, that were not included, that can be discussed at another meeting.  
(WF): The "Parking Lot" is a technique that is used to capture topics, that were not included, that can be discussed at another meeting.
(STARTIGNORE)
(Hint):
(Subject): Meetings
(Difficulty): Intermediate
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category):
(Grade style): 0
(Random answers): 1
(Question): Which of the following is the best way to deal with adversity?
(A): Make sure you do whatever you can to get your point across.
(B): Point your finger to the recipient, so you can show him you are the expert.
(C): Be friendly, always keep your temper!
(D): Documenting the conflict is not necessary.
(E): If two issues exist, then nest them together.
(Correct): C
(Points): 1
(CF): Losing your temper will only escalate the conflict and relationship.
(WF): Losing your temper will only escalate the conflict and relationship.
(STARTIGNORE)
(Hint):
(Subject): People
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 6
(Grade style): 0
(Random answers): 1
(Question): Which technique is not effective for engaging in a Cross-Cultural communication?
(A): Speak as loudly as you can, even if you have to yell.
(B): Avoid humor.
(C): Learn about the culture of the company and the country where it resides.
(D): Avoid slang.
(E): Separate the questions.
(Correct): A
(Points): 1
(CF): Speaking loudly and yelling will only degrade the dialogue.
(WF): Speaking loudly and yelling will only degrade the dialogue.
(STARTIGNORE)
(Hint):
(Subject): People
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)



